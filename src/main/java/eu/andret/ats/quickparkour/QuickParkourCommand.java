/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour;

import eu.andret.arguments.AnnotatedCommandExecutor;
import eu.andret.arguments.api.annotation.Argument;
import eu.andret.arguments.api.annotation.BaseCommand;
import eu.andret.arguments.api.entity.ExecutorType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@BaseCommand("quickparkour")
public class QuickParkourCommand extends AnnotatedCommandExecutor<QuickParkourPlugin> {
	public QuickParkourCommand(final CommandSender sender, final QuickParkourPlugin plugin) {
		super(sender, plugin);
	}

	@Argument(permission = "ats.quickParkour.get", description = "Gives main item", executorType = ExecutorType.PLAYER)
	public String get() {
		final Player player = (Player) sender;
		player.getInventory().addItem(plugin.getMainQuickParkourItem().toItemStack());
		return "Gave main item";
	}
}
