/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour.item;

public enum QuickParkourItemObjective {
	MAIN,
	SERVER,
	PLAYERS,
	TRAINING,
	EXIT,
	GLASS,
	PARKOUR
}
