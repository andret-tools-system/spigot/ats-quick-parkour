/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class ColorMapping {
	private final Map<DyeColor, Material> mapping = Map.ofEntries(
			Map.entry(DyeColor.BLACK, Material.BLACK_WOOL),
			Map.entry(DyeColor.BLUE, Material.BLUE_WOOL),
			Map.entry(DyeColor.BROWN, Material.BROWN_WOOL),
			Map.entry(DyeColor.CYAN, Material.CYAN_WOOL),
			Map.entry(DyeColor.GRAY, Material.GRAY_WOOL),
			Map.entry(DyeColor.GREEN, Material.GREEN_WOOL),
			Map.entry(DyeColor.LIGHT_BLUE, Material.LIGHT_BLUE_WOOL),
			Map.entry(DyeColor.LIGHT_GRAY, Material.LIGHT_GRAY_WOOL),
			Map.entry(DyeColor.LIME, Material.LIME_WOOL),
			Map.entry(DyeColor.MAGENTA, Material.MAGENTA_WOOL),
			Map.entry(DyeColor.ORANGE, Material.ORANGE_WOOL),
			Map.entry(DyeColor.PINK, Material.PINK_WOOL),
			Map.entry(DyeColor.PURPLE, Material.PURPLE_WOOL),
			Map.entry(DyeColor.RED, Material.RED_WOOL),
			Map.entry(DyeColor.WHITE, Material.WHITE_WOOL),
			Map.entry(DyeColor.YELLOW, Material.YELLOW_WOOL)
	);

	@NotNull
	public Material getMaterial(@NotNull final DyeColor color) {
		return mapping.get(color);
	}
}
