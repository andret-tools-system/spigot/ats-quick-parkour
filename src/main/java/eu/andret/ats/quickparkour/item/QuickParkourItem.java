/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour.item;

import eu.andret.ats.parkour.item.ParkourItem;
import eu.andret.ats.quickparkour.QuickParkourPlugin;
import lombok.Data;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataHolder;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

@Data
public class QuickParkourItem implements ParkourItem {
	private static final String DISABLED = "disabled";
	private static final String OBJECTIVE = "objective";

	@NotNull
	protected final QuickParkourPlugin plugin;
	@NotNull
	protected final ItemStack itemStack;
	@NotNull
	private final NamespacedKey objectiveKey;
	@NotNull
	private final NamespacedKey disabledKey;

	public QuickParkourItem(@NotNull final QuickParkourPlugin plugin, @NotNull final Material material) {
		this(plugin, new ItemStack(material));
	}

	public QuickParkourItem(@NotNull final QuickParkourPlugin plugin, @NotNull final ItemStack itemStack) {
		this.plugin = plugin;
		this.itemStack = itemStack;
		objectiveKey = new NamespacedKey(plugin, OBJECTIVE);
		disabledKey = new NamespacedKey(plugin, DISABLED);
	}

	public void setName(final String name) {
		Optional.of(itemStack)
				.map(ItemStack::getItemMeta)
				.ifPresent(itemMeta -> itemMeta.setDisplayName(name));
	}

	public void setLore(final List<String> lore) {
		Optional.of(itemStack)
				.map(ItemStack::getItemMeta)
				.ifPresent(itemMeta -> itemMeta.setLore(lore));
	}

	public void setGlowing(final boolean glowing) {
		if (glowing) {
			itemStack.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
		} else {
			itemStack.removeEnchantment(Enchantment.DURABILITY);
		}
	}

	public void setObjective(final QuickParkourItemObjective objective) {
		setPersistentData(objectiveKey, PersistentDataType.STRING, objective.name());
	}

	@Nullable
	public QuickParkourItemObjective getObjective() {
		return getPersistentData(objectiveKey, PersistentDataType.STRING)
				.map(QuickParkourItemObjective::valueOf)
				.orElse(null);
	}

	public boolean isDisabled() {
		return getPersistentData(disabledKey, PersistentDataType.BYTE)
				.map(disabled -> disabled != 0)
				.orElse(false);
	}

	public void setDisabled(final boolean disabled) {
		setPersistentData(disabledKey, PersistentDataType.BYTE, (byte) (disabled ? 1 : 0));
	}

	public void hideFlags() {
		Optional.of(itemStack)
				.map(ItemStack::getItemMeta)
				.ifPresent(x -> x.addItemFlags(ItemFlag.values()));
	}

	@NotNull
	@Override
	public ItemStack toItemStack() {
		return itemStack;
	}

	@NotNull
	protected <T, Z> Optional<Z> getPersistentData(@NotNull final NamespacedKey namespacedKey,
												   @NotNull final PersistentDataType<T, Z> type) {
		return Optional.of(itemStack)
				.map(ItemStack::getItemMeta)
				.map(PersistentDataHolder::getPersistentDataContainer)
				.filter(persistentDataContainer -> persistentDataContainer.has(namespacedKey, type))
				.map(persistentDataContainer -> persistentDataContainer.get(namespacedKey, type));
	}

	protected <T, Z> void setPersistentData(@NotNull final NamespacedKey namespacedKey,
											@NotNull final PersistentDataType<T, Z> type,
											@NotNull final Z data) {
		Optional.of(itemStack)
				.map(ItemStack::getItemMeta)
				.map(PersistentDataHolder::getPersistentDataContainer)
				.ifPresent(persistentDataContainer -> persistentDataContainer.set(namespacedKey, type, data));
	}
}
