/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour.item;

import eu.andret.ats.quickparkour.QuickParkourPlugin;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ParkourGameItem extends QuickParkourItem {
	@Nullable
	private ParkourGameItemData parkourGameItemData;
	@NotNull
	private final NamespacedKey parkourKey;

	public ParkourGameItem(@NotNull final QuickParkourPlugin plugin, @NotNull final ItemStack itemStack) {
		super(plugin, itemStack);
		parkourKey = new NamespacedKey(plugin, "parkour");
	}

	public void setParkour(@NotNull final String parkourName) {
		setPersistentData(parkourKey, PersistentDataType.STRING, parkourName);
	}

	@Nullable
	public String getParkour() {
		return getPersistentData(parkourKey, PersistentDataType.STRING).orElse(null);
	}
}
