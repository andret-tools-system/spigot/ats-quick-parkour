/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour.item;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
@NonFinal
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ParkourGameItemData extends ParkourGameItemGlobalData {
	int count;
	double personalTime;
	String lastComplete;

	public ParkourGameItemData(final double globalTime, final int count, final double personalTime, final String lastComplete) {
		super(globalTime);
		this.count = count;
		this.personalTime = personalTime;
		this.lastComplete = lastComplete;
	}
}
