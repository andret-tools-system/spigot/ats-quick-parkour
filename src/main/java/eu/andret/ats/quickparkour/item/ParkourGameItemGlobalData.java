/*
 * Copyright Andret (c) 2019-2022. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.quickparkour.item;

import lombok.Value;
import lombok.experimental.NonFinal;

@Value
@NonFinal
public class ParkourGameItemGlobalData {
	double globalTime;
}
